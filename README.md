# bash-opera_codecs_install

This script automatically sets up latest versions of ffmpeg and widevine for Opera.

```
bash ./opera-codecs-install.sh
```

When you want to listen to Spotify or watch video with the best browser (Opera) but it fails to load Chrome codecs.


## Supported flags

|flag | meaning|
--- | ---
|`-d`| download without install|
|`-f`| force redownload and install|

---

## TODO:
* kernel update hooks (for autoinstall)
