#!/bin/sh

OpCo_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ "$1" == "-d" ]; then
  echo '[DOWNLOAD ONLY MODE]'
elif [ "$1" == "-f" ]; then
  echo '[FORCED INSTALL MODE]'
else
  echo '(use flag "-d" to download without install or "-f" to force redownload and install)'
fi

ffmpeg_last_tag () {
  curl $1 2>&1 | grep 'linux-x64.zip' -m1 | cut -d'"' -f4 | cut -d'-' -f1
}

ffmpeg_download () {
  wget https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases/download/$1/$1-linux-x64.zip -q --show-progress
}

ffmpeg_install () {
  unzip -q $1-linux-x64.zip

  echo
  echo 'Installing FFMPEG...'
  sudo mv libffmpeg.so /usr/lib64/libffmpeg_h264.so
  sudo mv /usr/lib64/opera/libffmpeg.so /usr/lib64/opera/libffmpeg.so.orig
  sudo ln -s /usr/lib64/libffmpeg_h264.so /usr/lib64/opera/libffmpeg.so
  echo
}

widevine_last_version () {
  wget -O - -o /dev/null $1 | tail -n1
}

widevine_download () {
  wget https://dl.google.com/widevine-cdm/$1-linux-x64.zip -q --show-progress
}

widevine_install () {
  unzip -q $1-linux-x64.zip
  mkdir WidevineCdm
  mv manifest.json WidevineCdm
  mkdir 'WidevineCdm/_platform_specific'
  mkdir 'WidevineCdm/_platform_specific/linux_x64'
  mv libwidevinecdm.so 'WidevineCdm/_platform_specific/linux_x64'

  echo
  echo 'Installing WCDM...'
  sudo rm -r /opt/google/chrome/WidevineCdm
  sudo mkdir -p /opt/google/chrome
  sudo mv WidevineCdm /opt/google/chrome
  rm LICENSE.txt
  echo
}

existing_version () {
  ls -t $1 | grep -- "-linux-x64.zip" | cut -d'-' -f1
}

# get versions

ffmpeg_version="$(ffmpeg_last_tag https://api.github.com/repos/iteufel/nwjs-ffmpeg-prebuilt/releases)"
widevine_version="$(widevine_last_version https://dl.google.com/widevine-cdm/versions.txt)"

echo
echo '  Latest versions:'
echo '-----------------------'
echo " FFMPEG : $ffmpeg_version"
echo " WCDM   : $widevine_version"
echo '-----------------------'
echo

# ffmpeg

dir_ffmpeg=$OpCo_DIR/OperaCodecs/ffmpeg

ffmpeg_version_here="$(existing_version $dir_ffmpeg)"

cd $dir_ffmpeg
if [ "$1" == "-f" ] || [ "$ffmpeg_version_here" != "$ffmpeg_version" ]; then
  rm ./*
  echo 'Downloading FFMPEG...'
  ffmpeg_download $ffmpeg_version
else
  echo "FFMPEG version : $ffmpeg_version_here"
fi

if [ "$1" == "-d" ]; then
  :
else
  ffmpeg_install $ffmpeg_version
fi

# WidevineCDM

dir_widevine=$OpCo_DIR/OperaCodecs/widevine

widevine_version_here="$(existing_version $dir_widevine)"

cd $dir_widevine
if [ "$1" == "-f" ] || [ "$widevine_version_here" != "$widevine_version" ]; then
  rm ./*
  echo 'Downloading WCDM...'
  widevine_download $widevine_version
else
  echo "WCDM version   : $widevine_version_here"
fi

if [ "$1" == "-d" ]; then
  :
else
  widevine_install $widevine_version
fi

echo 'Done.'
